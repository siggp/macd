#pragma warning(disable : 4996)
#include <iostream>
#include <stdio.h>

using namespace std;

int countFloatsInTxtFile(const char* fileName) {
	FILE* file;
	int numberOfRecords = 0;
	float singleValue;

	file = fopen(fileName, "r");
	while (fscanf(file, "%f", &singleValue) > 0) numberOfRecords++;
	fclose(file);

	return numberOfRecords;
}

float* getDataFromTxtFile(const char* fileName, int numberOfRecords) {
	FILE* file;
	float* data;
	float singleValue;
	data = new float[numberOfRecords];

	file = fopen(fileName, "r");
	for (int i = 0; i < numberOfRecords; i++)
	{
		fscanf(file, "%f", &singleValue);
		data[i] = singleValue;
	}
	fclose(file);

	return data;
}

int saveArrayOfFloatsInTxtFile(const char* fileName, float* floatArray, int arrayLength) {
	FILE* file;
	file = fopen(fileName, "w");
	if (file == NULL)
	{
		cout << "Error in opening the file";
		return 0;
	}
	for (int i = 0; i < arrayLength; i++) fprintf_s(file, "%f\n", floatArray[i]);
}

float getAlpha(int recordNumber) {
	return (float)2 / (recordNumber - 1);
}

float getNumeratorOfCurrentEMA(float* records, int currentRecordNumber, int numberOfRecordsToAnalyse) {
	float numerator = 0;
	float factor = 1 - getAlpha(numberOfRecordsToAnalyse);


	for (int i = 0; i < numberOfRecordsToAnalyse && i <= currentRecordNumber; i++)
	{
		numerator += pow(factor, i) * records[currentRecordNumber - i];
	}


	return numerator;
}

float getDenumeratorOfCurrentEMA(int currentRecordNumber, int numberOfRecordsToAnalyse) {
	float denumerator = 0;
	float factor = 1 - getAlpha(numberOfRecordsToAnalyse);

	for (int i = 0; i < numberOfRecordsToAnalyse && i <= currentRecordNumber; i++)
	{
		denumerator += pow(factor, i);
	}

	return denumerator;
}

float* generateEMA_N(float* records, int numberOfRecords, int numberOfRecordsToAnalyse) {
	float* EMA_N = new float[numberOfRecords];

	for (int i = 0; i < numberOfRecords; i++)
	{
		EMA_N[i] = getNumeratorOfCurrentEMA(records, i, numberOfRecordsToAnalyse) /
			getDenumeratorOfCurrentEMA(i, numberOfRecordsToAnalyse);
				
	}

	return EMA_N;
}

float* generateFactorMACD(float* records, int numberOfRecords) {
	float* factorMACD = new float[numberOfRecords];
	float* EMA_12 = generateEMA_N(records, numberOfRecords, 12);
	float* EMA_26 = generateEMA_N(records, numberOfRecords, 26);

	for (int i = 0; i < numberOfRecords; i++)
	{
		factorMACD[i] = EMA_12[i] - EMA_26[i];
	}

	delete EMA_12;
	delete EMA_26;

	return factorMACD;
}

float* generateFactorSIGNAL(float* factorMACD, int numberOfRecords) {
	return generateEMA_N(factorMACD, numberOfRecords, 9);
}

float buy(float course, float cash) {
	return cash / course;
}

float sell(float course, float stock) {
	return stock * course;
}

float* countProfit(float initAmount, float* courses, float* factorsMACD, float* factorsSIGNAL, int numerOfRecords) {
	bool hasCash = true;
	float cash = initAmount;
	float stock = 0;
	int numberOfBuying = 0;
	int numberOfSelling = 0;

	for (int i = 0; i < numerOfRecords; i++)
	{
		if (factorsMACD[i] > factorsSIGNAL[i] && hasCash == true)	{
			stock = buy(courses[i], cash);
			cash = 0;
			hasCash = false;
			numberOfBuying++;
		}
		else if (factorsMACD[i] < factorsSIGNAL[i] && hasCash == false) {
			cash = sell(courses[i], stock);
			stock = 0;
			hasCash = true;
			numberOfSelling++;
		}
	}
	if (!hasCash)
	{
		cash = sell(courses[numerOfRecords - 1], stock);
		numberOfSelling++;
	}

	float result[3] = {
		cash - initAmount,
		(float)numberOfBuying,
		(float)numberOfSelling
	};

	return result;
}

void countProfitDemo(float* courses, float* factorsMACD, float* factorsSIGNAL, int numerOfRecords) {
	float initAmount = 1000000.00;
	float* countProfitResult = countProfit(initAmount, courses, factorsMACD, factorsSIGNAL, numerOfRecords);
	float profit = countProfitResult[0];
	int numberOfBuying = countProfitResult[1];
	int numberOfSelling = countProfitResult[1];
	int profitInPercent = profit * 100 / initAmount;

	cout << endl << "Przyklad wykorzystania" << endl
		<< "Kupowanie i sprzedawanie w momencie przeciecia SIGNAL i MACD" << endl
		<< "Kwota wejsciowa: " << initAmount << " PLN" << endl
		<< "Profit = " << profit << " (" << profitInPercent << " %)" << endl
		<< "Kupiono: " << numberOfBuying << ", Sprzedano: " << numberOfSelling << endl;	
}

void runApp() {
	int numberOfRecords = countFloatsInTxtFile("data.txt");
	float* records = getDataFromTxtFile("data.txt", numberOfRecords);
	float* factorMACD = generateFactorMACD(records, numberOfRecords);
	float* factorSIGNAL = generateFactorSIGNAL(factorMACD, numberOfRecords);

	countProfitDemo(records, factorMACD, factorSIGNAL, numberOfRecords);
	saveArrayOfFloatsInTxtFile("MACD.txt", factorMACD, numberOfRecords);
	saveArrayOfFloatsInTxtFile("SIGNAL.txt", factorSIGNAL, numberOfRecords);

	delete records;
	delete factorMACD;
	delete factorSIGNAL;
}