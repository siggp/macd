#pragma once
#include <iostream>
#include "Application.h"

using namespace std;

bool areTestsPassed = true;

bool areFloatsEqualIn5Precition(float x, float y) {
	if (x - y > -0.0000095 && x - y < 0.0000095) return true;
	return false;
}

void writeTestResult(const char* testName, bool isPassed) {
	const char* result = isPassed == true ? "Passed" : "Failed";
	cout << result << " " << testName << endl;
}

void writeFailedPrompt(float value, float correctValue) {
	areTestsPassed = false;
	const char* result = areFloatsEqualIn5Precition(value, correctValue) ? "Passed" : "Failed";
	cout << "\t" << result << " " << "Should be " << correctValue << " was " << value << endl;
}

void makeTest(const char* testName, float* values, float *correctValues, int numberOfTests) {
	bool isPassed = true;

	for (int i = 0; i < numberOfTests; i++)
	{
		isPassed = areFloatsEqualIn5Precition(values[i], correctValues[i]) ? isPassed : 0;
	}

	writeTestResult(testName, isPassed);
	if (isPassed == 0)
	{
		for (int i = 0; i < numberOfTests; i++)
		{
			writeFailedPrompt(values[i], correctValues[i]);
		}
	}
}

void getAlphaTest(int numberOfRecords) {
	int numberOfTests = 1;

	float values[] = {
		getAlpha(numberOfRecords)
	};

	float correctValues[] = { 0.5 };

	makeTest("GetAlphaTest", values, correctValues, numberOfTests);
}

void getNumeratorOfCurrentEMATest(float* records, int numberOfRecords) {
	int numberOfTests = 5;
	
	float values[] = {
		getNumeratorOfCurrentEMA(records, 0, 5),
		getNumeratorOfCurrentEMA(records, 1, 5),
		getNumeratorOfCurrentEMA(records, 2, 5),
		getNumeratorOfCurrentEMA(records, 3, 5),
		getNumeratorOfCurrentEMA(records, 4, 5)
	};

	float correctValues[] = { 1, 2.5, 4.25, 6.675, 4.3375 };
		
	makeTest("GetNumeratorOfCurrentEMATest", values, correctValues, numberOfTests);
}

void getDenumeratorOfCurrentEMATest(float* records, int numberOfRecords) {
	int numberOfTests = 5;

	float values[] = {
		getDenumeratorOfCurrentEMA(0, 5),
		getDenumeratorOfCurrentEMA(1, 5),
		getDenumeratorOfCurrentEMA(2, 5),
		getDenumeratorOfCurrentEMA(3, 5),
		getDenumeratorOfCurrentEMA(4, 5),
	};

	float correctValues[] = { 1, 1.5, 1.75, 1.875, 1.9375 };

	makeTest("GetDenumeratorOfCurrentEMATest", values, correctValues, numberOfTests);
}

void countFloatsInTxtFileTest() {
	int numberOfTests = 1;

	float values[] = {
		countFloatsInTxtFile("test.txt")
	};

	float correctValues[] = { 1000 };

	makeTest("CountFloatsInTxtFileTest", values, correctValues, numberOfTests);
}

void getDataFromTxtFileTest() {
	int numberOfTests = 2;

	float* result = getDataFromTxtFile("test.txt", 1000);
	float values[] = {
		result[0],
		result[999]
	};

	float correctValues[] = { 3.8, 3.5 };

	makeTest("GetDataFromTxtFileTest", values, correctValues, numberOfTests);
}

void generateEMA_NTest(float* records, int numberOfRecords, int numberOfRecordsToAnalyse) {
	int numberOfTests = 5;

	float* result = generateEMA_N(records, numberOfRecords, numberOfRecordsToAnalyse);

	float values[] = {
		result[0],
		result[1],
		result[2],
		result[3],
		result[4],
	};

	float correctValues[] = { 1, 1.66666, 2.42857, 3.56, 2.23871 };

	makeTest("GenerateEMA_NTest", values, correctValues, numberOfTests);
}

bool runTests() {
	int numberOfRecords = 5;
	float* records = new float[numberOfRecords];
	records[0] = 1;
	records[1] = 2;
	records[2] = 3;
	records[3] = 4.55;
	records[4] = 1;

	countFloatsInTxtFileTest();
	getDataFromTxtFileTest();
	getAlphaTest(numberOfRecords);
	getNumeratorOfCurrentEMATest(records, numberOfRecords);
	getDenumeratorOfCurrentEMATest(records, numberOfRecords);
	generateEMA_NTest(records, numberOfRecords, 5);

	return areTestsPassed;
}