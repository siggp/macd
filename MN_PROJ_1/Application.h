#ifndef Application_h
#define Application_h

int countFloatsInTxtFile(const char* fileName);
float* getDataFromTxtFile(const char* fileName, int numberOfRecords);
float getAlpha(int numberOfPeriods);
float getNumeratorOfCurrentEMA(float* records, int currentRecordNumber, int numberOfRecordsToAnalyse);
float getDenumeratorOfCurrentEMA(int currentRecordNumber, int numberOfRecordsToAnalyse);
float* generateEMA_N(float* records, int numberOfRecords, int numberOfRecordsToAnalyse);
void runApp();

#endif