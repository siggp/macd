# Required .txt files

This application requires two files with .txt extension:
	- test.txt
	- data.txt

test.txt file requirements:
	- must to be consist of exactly 1000 floats numers
	- first numer must be equal to 3.8
	- last number must be equal to 3.5
	- other 998 number are random numbers

In data.txt you should put your data to analyse (any number of float numbers)

# Application output

If test.txt and data.txt files are valid (meet rules above), application will
generate two files with results:
	- MACD.txt with MACD factor values
	- SYGNAL.txt with SYGNAL factor values

For further analysis, use these files in other programs (Python, Excel, Matlab etc.)